﻿using System;
using MoonRPG.Characters.Generator;
using MoonRPG.Moves.Controller;
namespace MoonRPG.Player
{
    public class HumanPlayer:BasePlayer
    {
        public HumanPlayer(GetPlayer GetOpponent)
            : base(new RandomCharacterGenerator(), GetOpponent)
        {
            this.moveController = new SelectableMoveController(this.GetCharacter, this.GetEnemyCharacter);
        }
    }
}

﻿using System;
using MoonRPG.Characters.Generator;
using MoonRPG.Moves.Controller;
namespace MoonRPG.Player
{
    public class ComputerPlayer:BasePlayer
    {
        public ComputerPlayer(GetPlayer GetOpponent)
            :base(new RandomCharacterGenerator(), GetOpponent)
        {
            this.moveController = new RandomMoveController(this.GetCharacter, this.GetEnemyCharacter);
        }
    }
}

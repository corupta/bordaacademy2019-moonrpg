﻿using System;
using MoonRPG;
using MoonRPG.Moves;
using MoonRPG.Characters.Generator;
using MoonRPG.Moves.Controller;
namespace MoonRPG.Player
{
    public delegate BasePlayer GetPlayer();

    public class BasePlayer
    {
        private static readonly object characterDeathLock = new object();

        public VisibleVertical VisibleCharacter { get; private set; }

        protected CharacterGenerator characterGenerator;

        protected Character character;
        protected MoveController moveController;

        protected GetPlayer GetOpponent;

        public BasePlayer(CharacterGenerator characterGenerator, GetPlayer GetOpponent)
        {
            this.characterGenerator = characterGenerator;
            this.GetOpponent = GetOpponent;
        }

        protected Character GetCharacter()
        {
            return this.character;
        }

        protected Character GetEnemyCharacter()
        {
            return this.GetOpponent()?.GetCharacter();
        }

        public void Prepare()
        {
            // todo use visibleverticalstatic instead !!!
            this.VisibleCharacter = new VisibleVerticalStatic(ConsoleFrame.maxX / 2);
            this.GetOpponent().moveController.MoveDone += this.NextMove;
            this.HandleCharacterDied(null);
        }

        private void HandleCharacterDied(Character deadCharacter)
        {
            lock (characterDeathLock)
            {
                if (deadCharacter != null)
                {
                    deadCharacter.CharacterDied -= this.HandleCharacterDied;
                    if (this.GetEnemyCharacter() != null)
                    {
                        this.GetEnemyCharacter().HandleKilled(deadCharacter);
                    }
                }
                character = characterGenerator.MakeNewCharacter();
                character.SetVisibleCharacter(VisibleCharacter);
                character.CharacterDied += this.HandleCharacterDied;
                character.Prepare();
            }
        }

        public void NextMove(Move move = null, Character caster = null, Character enemy = null)
        {
            lock (characterDeathLock)
            {
                this.moveController.MakeMove();
            }
        }
    }
}

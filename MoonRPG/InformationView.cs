﻿using System;
namespace MoonRPG
{
    public class InformationView:VisibleVerticalStatic
    {
        // public override int FrameWidth => ConsoleFrame.maxX;
        public override int FrameHeight => 4;
        private static InformationView instance;
        private static readonly object instanceLock = new object();
        public static InformationView GetInstance()
        {
            if (instance == null)
            {
                lock(instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new InformationView();
                    }
                }
            }
            return instance;
        }

        private InformationView() : base(ConsoleFrame.maxX) {

        }

        public override void ClearChildren()
        {
            base.ClearChildren();
        }
        /*
        #region TEMPORARY
        protected override void ReloadContent()
        {

            if (this.frame is null)
            {
                this.frame = new Frame(this.FrameWidth, this.FrameHeight, new Color(ConsoleColor.White, ConsoleColor.Red));
            }
        }
        #endregion
        */
    }
}

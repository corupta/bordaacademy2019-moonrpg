﻿using System;
using System.IO;
namespace MoonRPG
{
    public static class FileInputter
    {
        private static readonly string currentDirectory = "/Users/corupta/Projects/BordaAcademy2019/MoonRPG/MoonRPG";
        private static string GetFullPath(string relativePath)
        {
            string seperator = relativePath[0] == '/' ? "" : "/";
            return $"{currentDirectory}{seperator}{relativePath}";
        }
        public static string[] ReadAllLines(string relativePath)
        {

            return File.ReadAllLines(GetFullPath(relativePath));
        }
    }
}

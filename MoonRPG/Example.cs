﻿using System;
namespace MoonRPG
{
    public class Example
    {
        public Example()
        {
            DateTime dat = DateTime.Now;
            Console.WriteLine("The time: {0:d} at {0:t}", dat);
            TimeZoneInfo tz = TimeZoneInfo.Local;
            Console.WriteLine("The time zone: {0}\n",
                              tz.IsDaylightSavingTime(dat) ?
                                 tz.DaylightName : tz.StandardName);
            Console.Write("Press <Enter> to exit... ");
            while (Console.ReadKey(true).Key != ConsoleKey.Enter) { }
        }
    }
}
﻿using System;
namespace MoonRPG
{
    public class EarthSoldier : Character
    {
        private static readonly Frame attackFrame = new Frame(new string[2]
        {
            "/\\",
            "\\/"
        });
        private static readonly Frame skillFrame = new Frame(new string[5]
        {
            "/\\",
            "\\/",
            "  ",
            "/\\",
            "\\/"
        });
        protected override Frame AttackFrame { get => attackFrame; }
        protected override Frame SkillFrame { get => skillFrame; }


        public EarthSoldier(int level = 1, int experience = 0) : base(level, experience)
        {
        }


    }
}


/*
 *  Moon ~ Justice/Love *
 *  Mercury ~ Water *
 *  Venus ~ Light *
 *  Earth ~ Earth *
 *  Mars ~ Fire *
 *  Jupiter ~ Thunder *
 *  Uranus  ~ Air *
 *
 *  Moon => 12/4/1
 * 
 *    Air  > Fire  > Light > Earth  > Thunder >  Water  >  Air
 *  Uranus > Mars  > Venus > Earth  > Jupiter > Mercury > Uranus
 *  12/5/0 > 9/5/1 > 8/4/2 > 12/3/2 > 18/3/1  > 16/4/0  > 12/5/0
 *
 *  
 */

﻿using System;
namespace MoonRPG.Characters.Generator
{
    public class RandomCharacterGenerator:CharacterGenerator
    {
        private static Random random = new Random();
        public override Character MakeNewCharacter()
        {
            int randomNumber = RandomCharacterGenerator.random.Next(0, 7);
            switch(randomNumber)
            {
                case 0:
                    return new MoonSoldier();
                case 1:
                    return new EarthSoldier();
                case 2:
                    return new JupiterSoldier();
                case 3:
                    return new MarsSoldier();
                case 4:
                    return new MercurySoldier();
                case 5:
                    return new UranusSoldier();
                case 6:
                    return new VenusSoldier();
            }
            throw new Exception("Weird error when generating a new random character");
        }
    }
}

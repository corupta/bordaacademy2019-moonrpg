﻿using System;
namespace MoonRPG.Characters.Generator
{
    public abstract class CharacterGenerator
    {
        public abstract Character MakeNewCharacter();
    }
}

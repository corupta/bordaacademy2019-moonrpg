﻿using System;
namespace MoonRPG.Characters.Stats
{
    public class Attack:Stat
    {
        private static readonly Random random = new Random();
        public Attack(int attackPerLevel, int currentLevel): base(attackPerLevel, currentLevel) { }

        private int GetMinimumAttackValue()
        {
            return this.GetStatValue(0, -1);
        }

        private int GetMaximumAttackValue()
        {
            return this.GetStatValue(0, 1);
        }

        public int GetRandomAttackValue()
        {
            return Attack.random.Next(this.GetMinimumAttackValue(), this.GetMaximumAttackValue() + 1);
            // + 1 to include max value
        }

        protected override void GetDisplayMessage(out string displayMessage, out Color displayColor)
        {
            displayMessage = $"Attack: {this.GetMinimumAttackValue()} - {this.GetMaximumAttackValue()}";
            displayColor = null;
        }
    }
}

﻿using System;
namespace MoonRPG.Characters.Stats
{
    public class Defense:Stat
    {

        private int GetDefenseValue()
        {
            return this.GetStatValue();
        }

        public int GetDamageAfterAttack(int attackPower)
        {
            return Math.Max(attackPower - this.GetDefenseValue(), 0);
        }

        public Defense(int defensePerLevel, int currentLevel): base(defensePerLevel, currentLevel) { }

        protected override void GetDisplayMessage(out string displayMessage, out Color displayColor)
        {
            displayMessage = $"Defense: {this.GetDefenseValue()}";
            displayColor = null;
        }
    }
}

﻿using System;
namespace MoonRPG.Characters.Stats
{
    public delegate void LevelUpHandler(int newLevel);

    public class Experience:Stat
    {
        private int currentExperience;
        // todo
        public int GetExperienceUntilNextLevel()
        {
            return this.GetStatValue(0, (int)Math.Pow(1.5, this.currentLevel));
        }

        public void WinBattle(int enemyLevel)
        {
            this.currentExperience += (int) (Math.Pow(1.5, enemyLevel - this.currentLevel) * 10);
            this.ReloadAndAnimate();
            if (this.currentExperience > this.GetExperienceUntilNextLevel())
            {
                this.currentExperience -= this.GetExperienceUntilNextLevel();
                LevelUp?.Invoke(this.currentLevel + 1);
            }
        }

        public Experience(int currentLevel, int currentExperience): base(20, currentLevel)
        {
            this.currentExperience = currentExperience;
        }

        public event LevelUpHandler LevelUp;

        protected override void GetDisplayMessage(out string displayMessage, out Color displayColor)
        {
            string experienceBar = Bar.MakeStatBar(this.GetExperienceUntilNextLevel(), this.currentExperience);
            displayMessage = $"Exp:  {experienceBar} {this.currentExperience}/{this.GetExperienceUntilNextLevel()}";
            displayColor = null;
        }
    }
}

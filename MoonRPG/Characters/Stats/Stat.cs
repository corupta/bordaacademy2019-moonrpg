﻿using System;
namespace MoonRPG
{
    public abstract class Stat:VisibleDynamicLine
    {
        protected readonly int statPerLevel;
        protected int currentLevel;
        private Animatable updateAnimatable;

        public Stat(int statPerLevel, int currentLevel)
        {
            this.statPerLevel = statPerLevel;
            this.currentLevel = currentLevel;
        }
        protected abstract void GetDisplayMessage(out string displayMessage, out Color displayColor);

        protected int GetStatValue(int extraLevel = 0, int extraStatPerLevel = 0)
        {
            return (this.statPerLevel + extraStatPerLevel) * (this.currentLevel + extraLevel);
        }


        public void ChangeLevel(int newLevel)
        {
            this.currentLevel = newLevel;
            this.ReloadAndAnimate();
        }

        protected void ReloadAndAnimate()
        {
            this.GetDisplayMessage(out string displayMessage, out Color displayColor);
            this.ChangeText(displayMessage, displayColor);
            this.UpdateAnimatable();
        }

        protected override void ReloadContent()
        {
            if (this.frame is null)
            {
                this.GetDisplayMessage(out string message, out Color color);
                this.ChangeText(message, color);
            }
        }

        private void UpdateAnimatable()
        {
            if (this.updateAnimatable is null)
            {
                this.updateAnimatable = new Animatable(this, Animation.Blink, 2);
                this.updateAnimatable.AnimationFinished += this.UpdateAnimatableFinish;
            }
            this.updateAnimatable.Start();
        }

        public void UpdateAnimatableFinish(Animatable animatable)
        {
            this.Show();
        }
    }
}

﻿using System;
namespace MoonRPG.Characters.Stats
{
    public class Level: Stat
    {
        public int GetLevelValue()
        {
            return this.GetStatValue();
        }

        public Level(int currentLevel): base(1, currentLevel)
        {
        }

        protected override void GetDisplayMessage(out string displayMessage, out Color displayColor)
        {
            displayMessage = $"Level: {this.GetLevelValue()}";
            displayColor = null;
        }
    }
}

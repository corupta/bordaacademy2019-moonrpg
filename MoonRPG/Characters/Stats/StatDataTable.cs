﻿using System;
namespace MoonRPG
{
    public class StatDataTable:DataTable<StatData>
    {
        private static StatDataTable instance;
        private static readonly object instanceLock = new object();

        public static StatDataTable GetInstance()
        {
            if (instance is null)
            {
                lock(instanceLock)
                {
                    if (instance is null)
                    {
                        instance = new StatDataTable();
                    }
                }
            }
            return instance;
        }

        protected override StatData MakeRowData(string[] rowData)
        {
            return new StatData(rowData);
        }

        private StatDataTable(): base("StatData") {}
    }
}

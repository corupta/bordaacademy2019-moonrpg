﻿using System;
using MoonRPG;
namespace MoonRPG.Characters.Stats
{
    public class Mana:Stat
    {
        private static readonly Color[] manaColors =
        {
            new Color(ConsoleColor.DarkBlue),
            new Color(ConsoleColor.DarkCyan),
            new Color(ConsoleColor.Blue),
            new Color(ConsoleColor.Cyan)
        };

        private int currentMana;

        // private bool reloadCycleActive = false;

        private int GetMaximumManaValue()
        {
            return this.GetStatValue(2);
        }

        /// <summary>
        /// Increases or decreases mana by up to given amount.
        /// If the amount is negative, it decreases health up to 0
        /// If the amount is positive, it increases health up to Max Health
        /// If amount is null, fully refills health.
        /// </summary>
        /// <param name="amount">null or int</param>
        public void ChangeMana(int? amount)
        {
            this.currentMana = Math.Max(0, Math.Min(this.GetMaximumManaValue(), this.currentMana + (amount ?? this.GetMaximumManaValue())));
            if (amount is null || amount > 0)
            {
                this.GetDisplayMessage(out string displayMessage, out Color displayColor);
                this.ChangeText(displayMessage, displayColor);
            }
            else
            {
                this.ReloadAndAnimate();
            }
        }

        public Mana(int manaPerLevel, int currentLevel): base(manaPerLevel, currentLevel)
        {
            this.ChangeMana(null);
        }

        public void Reload()
        {
            this.ChangeMana(1);
        }

        /* 
        public void StartManaReloadCycle()
        {
            reloadCycleActive = true;
            ReloadManaCycle();
        }

        public void StopManaReloadCycle()
        {
            reloadCycleActive = false;
        }

        private void ReloadManaCycle()
        {
            if (reloadCycleActive)
            {
                this.ChangeMana(1);
                EventTimer.ScheduleAction(60, this.ReloadManaCycle);
            }
        }

    */
        public bool CanCastMove(int manaCost)
        {
            return this.currentMana >= manaCost;
        }

        protected override void GetDisplayMessage(out string displayMessage, out Color displayColor)
        {
            string manaBar = Bar.MakeStatBar(this.GetMaximumManaValue(), this.currentMana);

            displayMessage = $"Mana: {manaBar} {this.currentMana}/{this.GetMaximumManaValue()}";


            displayColor = manaColors[0];
            for (int i = 1; i < manaColors.Length; ++i)
            {
                if (this.GetMaximumManaValue() * i <= this.currentMana * (manaColors.Length))
                {
                    displayColor = manaColors[i];
                }
            }
        }
    }
}

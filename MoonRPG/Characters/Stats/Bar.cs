﻿using System;
namespace MoonRPG.Characters.Stats
{
    public class Bar
    {
        private static readonly char[] blocks =
        {
            '▒', '▓', '█'
        };
        public static string MakeStatBar(int maxValue, int currentValue, int barLength = 10)
        {
            string bar = "";
            for (int i = 0; i < barLength; ++i)
            {
                char next = blocks[0];
                for (int j = 1; j < Bar.blocks.Length; ++j)
                {
                    if (maxValue * ((Bar.blocks.Length - 1) * i + j) <= currentValue * barLength * (Bar.blocks.Length - 1))
                    {
                        next = Bar.blocks[j];
                    }
                }
                bar = next + bar;
            }
            return bar;
        }
        private Bar() {}
    }
}

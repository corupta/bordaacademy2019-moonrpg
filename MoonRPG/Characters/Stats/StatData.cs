﻿using System;
namespace MoonRPG
{
    public class StatData
    {
        public readonly string CharacterType;
        public readonly int HealthPerLevel;
        public readonly int AttackPerLevel;
        public readonly int DefensePerLevel;
        public readonly int ManaPerLevel;
        public StatData(string[] rowData)
        {
            if (rowData.Length != 5)
            {
                throw new ArgumentException("rowData should be of length 5", nameof(rowData));
            }
            this.CharacterType = rowData[0];
            this.HealthPerLevel = Int32.Parse(rowData[1]);
            this.AttackPerLevel = Int32.Parse(rowData[2]);
            this.DefensePerLevel = Int32.Parse(rowData[3]);
            this.ManaPerLevel = Int32.Parse(rowData[4]);
        }
    }
}

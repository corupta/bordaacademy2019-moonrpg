﻿using System;
using MoonRPG.Characters.Stats;

namespace MoonRPG
{
    public class StatList:VisibleVerticalDynamic
    {
        public Level level;
        public Experience experience;
        public Health health;
        public Attack attack;
        public Defense defense;
        public Mana mana;
        public StatList(string characterType, int level, int experience)
        {
            StatData statData = StatDataTable.GetInstance().GetData(characterType);
            this.level = new Level(level);
            this.experience = new Experience(level, experience);
            this.health = new Health(statData.HealthPerLevel, level);
            this.mana = new Mana(statData.ManaPerLevel, level);
            this.attack = new Attack(statData.AttackPerLevel, level);
            this.defense = new Defense(statData.DefensePerLevel, level);

            this.experience.LevelUp += this.level.ChangeLevel;
            this.experience.LevelUp += this.experience.ChangeLevel;
            this.experience.LevelUp += this.health.ChangeLevel;
            this.experience.LevelUp += this.mana.ChangeLevel;
            this.experience.LevelUp += this.attack.ChangeLevel;
            this.experience.LevelUp += this.defense.ChangeLevel;

            this.AddChildrenToBottom(this.level);
            this.AddChildrenToBottom(this.experience);
            this.AddChildrenToBottom(this.health);
            this.AddChildrenToBottom(this.mana);
            this.AddChildrenToBottom(this.attack);
            this.AddChildrenToBottom(this.defense);

            this.health.Died += this.HandleDeath;

            // this.mana.StartManaReloadCycle();
        }

        private void HandleDeath()
        {
            // this.mana.StopManaReloadCycle();

            this.experience.LevelUp -= this.level.ChangeLevel;
            this.experience.LevelUp -= this.experience.ChangeLevel;
            this.experience.LevelUp -= this.health.ChangeLevel;
            this.experience.LevelUp -= this.mana.ChangeLevel;
            this.experience.LevelUp -= this.attack.ChangeLevel;
            this.experience.LevelUp -= this.defense.ChangeLevel;
        }
    }
}

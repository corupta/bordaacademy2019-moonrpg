﻿using System;
namespace MoonRPG.Characters.Stats
{
    public delegate void HealthEventHandler();

    public class Health : Stat
    {
        private static readonly Color[] healthColors =
        {
            new Color(ConsoleColor.Red),
            new Color(ConsoleColor.Yellow),
            new Color(ConsoleColor.Green),
        };

        private int currentHealth;

        private int GetMaximumHealthValue()
        {
            return this.GetStatValue(4);
        }

        /// <summary>
        /// Increases or decreases health by up to given amount.
        /// If the amount is negative, it decreases health up to 0
        /// If the amount is positive, it increases health up to Max Health
        /// If amount is null, fully refills health.
        /// </summary>
        /// <param name="amount">null or int</param>
        public void ChangeHealth(int? amount)
        {
            bool wasAlive = this.currentHealth > 0;
            this.currentHealth = Math.Max(0, Math.Min(this.GetMaximumHealthValue(), this.currentHealth + (amount ?? this.GetMaximumHealthValue())));
            if (amount is null)
            {
                this.GetDisplayMessage(out string displayMessage, out Color displayColor);
                this.ChangeText(displayMessage, displayColor);
            } else {
                this.ReloadAndAnimate();
            }
            if (wasAlive && this.currentHealth <= 0)
            {
                this.Died?.Invoke();
            }
        }

        protected override void GetDisplayMessage(out string displayMessage, out Color displayColor)
        {
            string healthBar = Bar.MakeStatBar(this.GetMaximumHealthValue(), this.currentHealth);

            displayMessage = $"HP:   {healthBar} {this.currentHealth}/{this.GetMaximumHealthValue()}";


            displayColor = healthColors[0];
            for (int i = 1; i < healthColors.Length; ++i)
            {
                if (this.GetMaximumHealthValue() * i <= this.currentHealth * (healthColors.Length))
                {
                    displayColor = Health.healthColors[i];
                }
            }
        }

        public Health(int healthPerLevel, int currentLevel) : base(healthPerLevel, currentLevel) {
            this.ChangeHealth(null);
        }

        public event HealthEventHandler Died;
    }
}

﻿using System;
using System.Collections.Generic;
using MoonRPG.Moves;

namespace MoonRPG
{
    public delegate void CharacterEvent(Character currentCharacter);

    public abstract class Character
    {
        private static readonly Move[] defaultMoves = {
            new AttackMove(),
            new DoubleAttackMove(),
            new Heal()
        };
        /*
        public override int FrameWidth => 16;
        public override int FrameHeight => 21;
        */

        public StatList statList;
        public MoveList moveList;
        public VisibleStaticLine characterName;
        private readonly Avatar avatar;
        protected abstract Frame AttackFrame { get; }
        protected abstract Frame SkillFrame { get; }

        protected Character(int level = 1, int experience = 0)
        {
            if (level < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(level), "A character can be at minimum level 1");
            }
            if (experience < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(experience), "A character's experience cannot be negative");
            }
            this.moveList = new MoveList(Character.defaultMoves);
            this.avatar = Avatar.GetAvatar(this.GetType().Name);
            this.characterName = new VisibleStaticLine(this.GetType().Name);
            this.statList = new StatList(this.GetType().Name, level, experience);
            // todo instead receive visibleCharacter in a seperate function
        }

        public void SetVisibleCharacter(VisibleVertical visibleCharacter)
        {
            visibleCharacter.ClearChildren();
            visibleCharacter.AddChildrenToBottom(this.avatar);
            visibleCharacter.AddChildrenToBottom(this.characterName);
            visibleCharacter.AddChildrenToBottom(this.statList);
        }

        public void Prepare()
        {
            this.statList.health.ChangeHealth(null);
            this.statList.health.Died += this.HandleDied;
        }

        /*
        public void SetEnemy(Character enemy, bool recurse = true)
        {
            if (enemy != null)
            {
                this.statList.health.Died += this.Die;
                enemy.CharacterDied += this.Kill;
                if (recurse)
                {
                    enemy.SetEnemy(this, false);
                }
            }
        }
        */

        private void HandleDied() {
            CharacterDied?.Invoke(this);
            this.statList.health.Died -= this.HandleDied;
        }

        public void HandleKilled(Character killedCharacter)
        {
            this.statList.experience.WinBattle(killedCharacter.statList.level.GetLevelValue());
            // todo maybe move this prepare to upper scope
            this.Prepare();
        }

        public event CharacterEvent CharacterDied;
    }
}

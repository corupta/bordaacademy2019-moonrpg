﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MoonRPG
{
    public class Avatar:VisibleStatic
    {
        private static Dictionary<string, Frame> avatarDictionary = new Dictionary<string, Frame>();
        public static Avatar GetAvatar(string avatarFile)
        {
            if(!avatarDictionary.ContainsKey(avatarFile))
            {
                Frame staticImage = new Frame(
                    FileInputter.ReadAllLines($"Assets/Avatars/{avatarFile}.txt")
                );
                avatarDictionary[avatarFile] = staticImage;
            }
            return new Avatar(new Frame(avatarDictionary[avatarFile]));
            // return avatarDictionary[avatarFile];
        }
        private Avatar(Avatar other): base(other.staticImage)
        {
        }
        private Avatar(Frame avatarImage):base(avatarImage)
        {
        }
    }
}

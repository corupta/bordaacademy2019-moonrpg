﻿using System;
using System.Collections.Generic;

namespace MoonRPG
{
    public abstract class VisibleContainer: Visible
    {
        private List<Visible> children;

        public VisibleContainer()
        {
            this.children = new List<Visible>();
        }

        public void AddChildren(Position childPosition, Visible child)
        {
            children.Add(child);
            child.position = childPosition;
            child.parent = this;
        }
    }
}

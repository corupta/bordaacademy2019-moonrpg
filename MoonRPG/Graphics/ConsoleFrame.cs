﻿using System;
namespace MoonRPG
{
    public class ConsoleFrame : Frame
    {
        private static ConsoleFrame instance;
        private static readonly object instanceLock = new object();
        public static readonly int maxX = 80;
        public static readonly int maxY = 24;
        public static readonly ConsoleColor defaultBackgroundColor = ConsoleColor.Black;
        public static readonly ConsoleColor defaultForegroundColor = ConsoleColor.White;

        public override bool CanPrintConsole { get { return true; } }

        public static ConsoleFrame GetInstance()
        {
            if (instance == null)
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new ConsoleFrame();
                    }
                }
            }
            return instance;
        }

        private ConsoleFrame() : base(ConsoleFrame.maxX, ConsoleFrame.maxY)
        {
            this.PrintFrame(Position.TopLeft, this, true);
            Console.CursorVisible = false;
        }

    }
}

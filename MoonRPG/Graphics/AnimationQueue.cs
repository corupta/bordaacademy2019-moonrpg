﻿using System;
using System.Collections.Generic;
namespace MoonRPG
{
    public class AnimationQueue
    {
        public Dictionary<Guid, Animatable> animatables;
        public List<Animatable> animatables;
        private static AnimationQueue instance;
        public static AnimationQueue GetInstance()
        {
            if (AnimationQueue.instance == null)
            {
                AnimationQueue.instance = new AnimationQueue();
            }
            return AnimationQueue.instance;
        }
        private AnimationQueue()
        {
            this.animatables = new List<Animatable>();
        }
        public void AddAnimatable(Animatable animatable)
        {
            this.animatables.Add(animatable);
        }
        public bool IsFinished ()
        {
            return animatables.Count == 0;
        }
        public void Next()
        {
            for (var i = animatables.Count - 1; i >= 0; --i)
            {
                if (animatables[i].Next() == false)
                {
                    this.animatables[i].Finish();
                    this.animatables.RemoveAt(i);
                }
            }
        }
    }
}

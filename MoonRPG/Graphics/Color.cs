﻿using System;
namespace MoonRPG
{
    public class Color
    {
        private readonly ConsoleColor fg = Printer.defaultForegroundColor;
        private readonly ConsoleColor bg = Printer.defaultBackgroundColor;

        public static bool operator== (Color c1, Color c2)
        {
            if (c1 is null || c2 is null)
            {
                return c1 is null && c2 is null;
            }
            return c1.fg == c2.fg && c1.bg == c2.bg;
        }

        public static bool operator!= (Color c1, Color c2)
        {
            return !(c1 == c2);
        }

        public Color() { }
        public Color(Color col)
        {
            this.fg = col.fg;
            this.bg = col.bg;
        }
        public Color(ConsoleColor fg)
        {
            this.fg = fg;
        }
        public Color(ConsoleColor fg, ConsoleColor bg)
        {
            this.fg = fg;
            this.bg = bg;
        }
        public void Paint()
        {
            Console.BackgroundColor = this.bg;
            Console.ForegroundColor = this.fg;
        }

        public override bool Equals(object obj)
        {
            return obj is Color color &&
                   fg == color.fg &&
                   bg == color.bg;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(fg, bg);
        }
    }
}

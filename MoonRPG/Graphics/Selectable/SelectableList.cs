﻿using System;
using System.Collections.Generic;
namespace MoonRPG.Graphics.Selectable
{
    public delegate void SelectEvent(Selectable selectable);

    public class SelectableList<T> where T:Selectable
    {
        private bool isActive;
        private int selectedIndex;
        protected readonly List<T> selectables;

        public SelectableList()
        {
            this.selectables = new List<T>();
        }

        public void AddSelectable(T selectable)
        {
            this.selectables.Add(selectable);
        }

        public void ClearChildren()
        {
            this.selectables.Clear();
        }

        protected virtual void Prepare() { }

        public void Activate()
        {
            if (isActive)
            {
                this.Disable();
            }

            this.Prepare();

            isActive = true;

            selectedIndex = -1 + selectables.Count;

            foreach (Selectable selectable in this.selectables) {
                selectable.Prepare();
            }

            this.NextSelectable(1);

            KeyInputter.GetInstance().KeyPressed += this.KeyPress;
        }

        protected virtual void Disable()
        {
            if (isActive)
            {
                isActive = false;
                Selectable currentSelectable = this.selectables[selectedIndex % selectables.Count];
                currentSelectable.UnHover();
                KeyInputter.GetInstance().KeyPressed -= this.KeyPress;
            }
        }

        private void MakeSelection()
        {
            if (isActive)
            {
                Selectable selectedSelectable = this.selectables[selectedIndex % selectables.Count];
                selectedSelectable.Select();
                this.Disable();
                this.OnSelected?.Invoke(selectedSelectable);
            }
        }

        private void NextSelectable(int increment)
        {
            if (isActive)
            {
                this.selectables[selectedIndex % selectables.Count].UnHover();
                do
                {
                    selectedIndex += increment;
                } while (this.selectables[selectedIndex % selectables.Count].Disabled == true);
                this.selectables[selectedIndex % selectables.Count].Hover();
            }
        }

        void KeyPress(ConsoleKey key)
        {
            if (isActive)
            {
                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        this.NextSelectable(-1);
                        break;
                    case ConsoleKey.DownArrow:
                        this.NextSelectable(1);
                        break;
                    case ConsoleKey.Enter:
                        this.MakeSelection();
                        break;
                }
            }
        }

        public event SelectEvent OnSelected;
    }
}

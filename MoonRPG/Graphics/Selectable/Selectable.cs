﻿using System;
using MoonRPG;
namespace MoonRPG.Graphics.Selectable
{
    public class Selectable
    {
        public MoonRPG.Visible Visible { get; private set; }
        private bool isHovered;
        public bool Disabled { get; protected set; }
        Animatable hoverAnimatable;
        public virtual void Prepare()
        {
            this.Disabled = false;
        }
        public Selectable(MoonRPG.Visible visible)
        {
            this.Visible = visible;
        }

        private void HoverAnimatable(Animatable animatable = null)
        {
            if (this.isHovered)
            {
                if (animatable is null)
                {
                    animatable = new Animatable(this.Visible, Animation.Blink, 4);
                    animatable.AnimationFinished += this.HoverAnimatable;
                }
                animatable.Start();
                this.hoverAnimatable = animatable;
            } else
            {
                this.Visible.Show();
            }
        }

        public void Hover()
        {
            if (this.isHovered == false)
            {
                this.isHovered = true;
                this.HoverAnimatable(this.hoverAnimatable);
            }
        }

        public void UnHover()
        {
            if (this.isHovered == true)
            {
                this.isHovered = false;
                this.hoverAnimatable.Finish();
            }
        }

        public void Select()
        {
            this.UnHover();
            this.HandleSelected();
        }

        protected virtual void HandleSelected() { }

    }
}

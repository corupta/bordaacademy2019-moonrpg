﻿using System;
namespace MoonRPG
{
    public class Position
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public Position(Position p)
        {
            this.X = p.X;
            this.Y = p.Y;
        }
        public Position(Position p, int x, int y)
        {
            this.X = p.X + x;
            this.Y = p.Y + y;
        }
        public Position(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        // may throw ArgumentOutOfRangeException
        public void MoveCursorTo()
        {
           Console.SetCursorPosition(X, Y);
        }

        public bool IsWithinBounds(int maxX, int maxY)
        {
            return this.X >= 0 && this.X < maxX && this.Y >= 0 && this.Y < maxY;
        }

        public static Position TopLeft = new Position(0, 0);
        public static Position BottomLeft = new Position(0, ConsoleFrame.maxY - 1);

        public static Position LeftPlayer = new Position(1, 1);

        public static Position RightPlayer = new Position(61, 1);

        public static Position CharacterPosition = new Position(0, 0);
        public static Position HealthPosition = new Position(1, 17);
        public static Position AttackPosition = new Position(1, 18);
        public static Position DefensePosition = new Position(1, 19);

        public static Position MovesPosition = new Position(1, 22);

    }
}

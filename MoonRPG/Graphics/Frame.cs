﻿using System;
namespace MoonRPG
{
    public class Frame
    {
        public virtual bool CanPrintConsole { get { return false; } }
        public Pixel[,] Pixels { get; private set; }
        public readonly int width;
        public readonly int height;
        public Frame(Frame f)
        {
            this.width = f.width;
            this.height = f.height;
            this.Pixels = new Pixel[height, width];
            for (var y = 0; y < height; ++y)
            {
                for (var x = 0; x < width; ++x)
                {
                    this.Pixels[y, x] = new Pixel(f.Pixels[y, x]);
                }
            }
        }
        public Frame(int width, int height, Color color = null)
        {
            this.width = width;
            this.height = height;
            this.Pixels = new Pixel[height, width];
            for (var y = 0; y < height; ++y)
            {
                for (var x = 0; x < width; ++x)
                {
                    this.Pixels[y, x] = new Pixel(' ', color);
                }
            }
        }
        public Frame(string[] s, Color color = null) : this(s[0].Length, s.Length, s, color) {}
        public Frame(int width, int height, string[] s, Color color = null)
        {
            this.width = width;
            this.height = height;
            this.Pixels = new Pixel[height, width];
            for (var y = 0; y < height; ++y)
            {
                if (s[y].Length != width)
                {
                    throw new ArgumentException("Each string of the String array passed to Frame() constructor must have the same length!", nameof(s));
                }
                for (var x = 0; x < width; ++x)
                {
                    this.Pixels[y, x] = new Pixel(s[y][x], color);
                }
            }
        }

        public Pixel PrintPixel(Position pos, Pixel p, bool force = false)
        {
                if (p == null || !pos.IsWithinBounds(this.width, this.height))
                {
                    return null;
                }
                Pixel prev = this.Pixels[pos.Y, pos.X];
                if ((prev != p) || force)
                {
                    if (this.CanPrintConsole)
                    {
                        p.Print(pos);
                    }
                    this.Pixels[pos.Y, pos.X] = new Pixel(p);
                }
                return prev;
        }

        public Frame PrintFrame(Position pos, Frame f, bool force = false)
        {
            if (f == null)
            {
                return null;
            }
            Frame prev = new Frame(f.width, f.height);
            for (var y = 0; y < f.height; ++y)
            {
                for (var x = 0; x < f.width; ++x)
                {
                    prev.Pixels[y, x] = this.PrintPixel(new Position(pos, x, y), f.Pixels[y, x], force);
                }
            }
            return prev;
        }
    }
}

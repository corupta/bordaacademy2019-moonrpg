﻿using System;
namespace MoonRPG
{
    public class Animation
    {
        /*
        public Animation Repeated(int repeatCount)
        {
            AnimationMove[] newMoves = new AnimationMove[this.Duration() * repeatCount];
            for (int i = 0; i < newMoves.Length; ++i)
            {
                newMoves[i] = this.GetAnimationMove(i);
            }
            return new Animation(newMoves);
        }
        */

        private readonly AnimationMove[] moves;
        public Animation(AnimationMove[] moves)
        {
            this.moves = moves;
        }

        public AnimationMove GetAnimationMove(int index)
        {
            return this.moves[index % this.moves.Length];
        }

        public int Duration()
        {
            return this.moves.Length;
        }

        public static Animation LinearRight = new Animation(new AnimationMove[] {
            new AnimationMove(1, 0)
        });
        public static Animation LinearLeft = new Animation(new AnimationMove[] {
            new AnimationMove(-1, 0)
        });
        public static Animation LinearUp = new Animation(new AnimationMove[] {
            new AnimationMove(0, -1)
        });
        public static Animation LinearDown = new Animation(new AnimationMove[] {
            new AnimationMove(0, 1)
        });
        public static Animation CircleClockWise = new Animation(new AnimationMove[] {
            new AnimationMove(1, 0), new AnimationMove(1, 0),
            new AnimationMove(1, 1), new AnimationMove(1, 0),
            new AnimationMove(1, 1), new AnimationMove(0, 1),
            new AnimationMove(1, 1), new AnimationMove(0, 1),
            new AnimationMove(0, 1), new AnimationMove(-1, 1),
            new AnimationMove(0, 1), new AnimationMove(-1, 1),
            new AnimationMove(-1, 0), new AnimationMove(-1, 1),
            new AnimationMove(-1, 0), new AnimationMove(-1, 0),
            new AnimationMove(-1, -1), new AnimationMove(-1, 0),
            new AnimationMove(-1, -1), new AnimationMove(0, -1),
            new AnimationMove(-1, -1), new AnimationMove(0, -1),
            new AnimationMove(0, -1), new AnimationMove(1, -1),
            new AnimationMove(0, -1), new AnimationMove(1, -1),
            new AnimationMove(1, 0), new AnimationMove(1, -1),
        });
        public static Animation Blink = new Animation(new AnimationMove[]
        {
            new AnimationMove(0, 0), new AnimationMove(0, 0),
            new AnimationMove(0, 0), new AnimationMove(-100, 0),
            new AnimationMove(0, 0), new AnimationMove(0, 0),
            new AnimationMove(0, 0), new AnimationMove(100, 0)
        });
    }
}

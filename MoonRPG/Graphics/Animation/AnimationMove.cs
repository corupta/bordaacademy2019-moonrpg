﻿using System;
namespace MoonRPG
{
    public class AnimationMove
    {
        private int incX;
        private int incY;
        public AnimationMove(int incX, int incY)
        {
            this.incX = incX;
            this.incY = incY;
        }
        public Position NextPosition(Position pos)
        {
            return new Position(pos, incX, incY);
        }
    }
}

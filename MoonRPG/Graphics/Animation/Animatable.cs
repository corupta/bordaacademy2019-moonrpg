﻿using System;
namespace MoonRPG
{
    public delegate void AnimationEventHandler(Animatable emitter);
    // public delegate void AnimationEventHandler();

    public class Animatable
    {
        private readonly Visible visible;
        private Animation animation;
        private int animationIndex;
        private int duration;
        private Position initialPosition;
        private bool isPlaying;
        public Animatable(Visible visible, Animation animation) : this(visible, animation, 1) { }
        public Animatable(Visible visible, Animation animation, int cycleCount) 
        {
            this.visible = visible;
            this.initialPosition = this.visible.Position;
            this.animation = animation;
            this.duration = cycleCount * animation.Duration();
        }
        public void Start()
        {
            if (isPlaying)
            {
                this.Finish();
            }
            isPlaying = true;
            this.animationIndex = -1; // so that the first frame would be printed at the initial position
            this.visible.Show();
            EventClock.GetInstance().ClockTick += this.PlayNextFrame;
        }
        public void PlayNextFrame()
        {
            if (this.animationIndex < this.duration) { 
                if (this.animationIndex > 0)
                {
                    this.visible.Hide();
                    AnimationMove nextMove = this.animation.GetAnimationMove(this.animationIndex);
                    this.visible.Position = nextMove.NextPosition(this.visible.Position);
                    this.visible.Show();
                }
                ++this.animationIndex;
            } else
            {
                this.Finish();
            }
        }
        public void Finish()
        {
            if (isPlaying)
            {
                isPlaying = false;
                EventClock.GetInstance().ClockTick -= this.PlayNextFrame;
                this.visible.Hide();
                this.visible.Position = this.initialPosition;
                AnimationFinished?.Invoke(this);
            }
        }

        public event AnimationEventHandler AnimationFinished;
    }
}

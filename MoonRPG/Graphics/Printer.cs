﻿using System;
namespace MoonRPG
{
    public class Printer:Visible
    {
        public static readonly ConsoleColor defaultBackgroundColor = ConsoleColor.Black;
        public static readonly ConsoleColor defaultForegroundColor = ConsoleColor.White;

        private static Printer instance;
        private static readonly object instanceLock = new object();
        public override int FrameWidth { get => ConsoleFrame.maxX; }
        public override int FrameHeight { get => ConsoleFrame.maxY; }

        private readonly ConsoleFrame consoleFrame;

        public void SetChildren(Visible child)
        {
            this.ClearChildren();
            this.AddChildren(Position.TopLeft, child);
        }

        public static Printer GetInstance()
        {
            if (instance == null)
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new Printer();
                    }
                }
            }
            return instance;
        }

        private Printer() {
            this.consoleFrame = ConsoleFrame.GetInstance();
            EventClock eventClock = EventClock.GetInstance();
            eventClock.ClockTick += this.CommitToScreen;
        }

        private void CommitToScreen()
        {
            this.Show();
            this.consoleFrame.PrintFrame(Position.TopLeft, this.frame);
        }
        
    }
}

﻿using System;
using System.Collections.Generic;

namespace MoonRPG
{
    public class Pixel
    {
        private readonly char ch;
        private readonly Color col;

        public static bool operator== (Pixel p1, Pixel p2)
        {
            if (p1 is null || p2 is null)
            {
                return p1 is null && p2 is null;
            }
            return p1.ch == p2.ch && p1.col == p2.col;
        }

        public static bool operator!= (Pixel p1, Pixel p2)
        {
            return !(p1 == p2);
        }
        public Pixel(Pixel p) : this(p.ch, new Color(p.col)) { }
        public Pixel(char ch, Color col = null)
        {
            this.ch = ch;
            this.col = col ?? new Color();
        }
        public void Print(Position pos)
        {
            try
            {
                pos.MoveCursorTo();
                this.col.Paint();
                Console.Write(this.ch);
            } catch (ArgumentOutOfRangeException)
            {
                // skip printing when it is out of range.
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Pixel pixel &&
                   ch == pixel.ch &&
                   EqualityComparer<Color>.Default.Equals(col, pixel.col);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ch, col);
        }
    }
}

﻿using System;
namespace MoonRPG
{
    public class VisibleVerticalStatic:VisibleVertical
    {
        public override int FrameWidth { get => frameWidth; }
        private int frameWidth;

        public VisibleVerticalStatic(int? frameWidth)
        {
            this.frameWidth = frameWidth ?? ConsoleFrame.maxX;
        }
    }
}

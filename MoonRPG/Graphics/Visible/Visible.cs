﻿using System;
using System.Collections.Generic;

namespace MoonRPG
{
    public abstract class Visible
    {
        public abstract int FrameWidth { get; }
        public abstract int FrameHeight { get; }
        protected Visible parent;
        protected Frame frame;
        protected Frame prevFrame;
        public Position Position { get; set; }
        protected List<Visible> children;

        protected Visible()
        {
            this.children = new List<Visible>();
        }

        protected void AddChildren(Position childPosition, Visible child)
        {
            children.Add(child);
            child.Position = childPosition;
            child.parent = this;
            child.prevFrame = null;
        }

        public virtual void ClearChildren()
        {
            this.Hide();
            foreach(var child in children)
            {
                child.parent = null;
            }
            children.Clear();
            this.frame = null;
            this.Show();
        }

        protected virtual void ReloadContent()
        {
            if (this.frame is null)
            {
                this.frame = new Frame(this.FrameWidth, this.FrameHeight);
            }
        }

        private void PrintToParent(bool chainParent = false, bool printCurrent = true)
        {
            if (this.parent is null)
            {
                return;
                // throw new Exception("Cannot print a Visible without providing a canvas");
            }
            if (this.prevFrame != null)
            {
                this.parent.frame.PrintFrame(this.Position, this.prevFrame);
            }
            this.parent.ReloadContent();
            if (printCurrent)
            {
                this.prevFrame = this.parent.frame.PrintFrame(this.Position, this.frame);
            }
            if (chainParent)
            {
                this.parent.PrintToParent(true);
            }
        }

        public void Show(bool chainParent = false)
        {
            this.ReloadContent();
            foreach (Visible child in children) {
                child.Show();
            }
            this.PrintToParent(chainParent);
        }
        public void Hide(bool chainParent = false)
        {
            if (frame == null) // no need to update if the frame is not initialized yet.
            {
                return;
            }
            /* this.frame = this.prevFrame;
            this.prevFrame = null; */
            this.PrintToParent(chainParent, false);
            this.prevFrame = null;
        }
    }
}

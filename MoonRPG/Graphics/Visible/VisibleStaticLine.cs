﻿using System;
namespace MoonRPG
{
    public class VisibleStaticLine:VisibleStatic
    {
        public VisibleStaticLine(string line, Color color = null) : base(new Frame(new string[1]{ line }, color)) {}
    }
}

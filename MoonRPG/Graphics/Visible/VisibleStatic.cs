﻿using System;
namespace MoonRPG
{
    public abstract class VisibleStatic : Visible
    {
        protected readonly Frame staticImage;

        public override int FrameWidth => staticImage.width;
        public override int FrameHeight => staticImage.height;

        protected VisibleStatic(Frame staticImage)
        {
            this.staticImage = staticImage;
        }

        protected override void ReloadContent()
        {
            if (this.frame is null)
            {
                this.frame = new Frame(this.staticImage);
            }
        }
    }
}

﻿using System;
using System.Linq;

namespace MoonRPG
{
    public class VisibleVerticalDynamic:VisibleVertical
    {
        public override int FrameWidth { get => maxChildWidth; }
        private int maxChildWidth;

        public VisibleVerticalDynamic(){
            this.maxChildWidth = 0;
        }

        protected override void ReloadContent()
        {
            try
            {
                this.maxChildWidth = this.children.Max(visible => visible.FrameWidth);
            } catch (InvalidOperationException)
            {
                this.maxChildWidth = 0;
            }
            base.ReloadContent();
        }

        public override void ClearChildren()
        {
            base.ClearChildren();
            this.maxChildWidth = 0;
        }

    }
}

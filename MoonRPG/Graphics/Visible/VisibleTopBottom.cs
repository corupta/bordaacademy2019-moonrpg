﻿using System;
namespace MoonRPG
{
    public abstract class VisibleTopBottom : Visible
    {
        protected Visible topChild;
        protected Visible bottomChild;

        public void SetChildren(Visible topChild, Visible bottomChild)
        {
            this.ClearChildren();
            this.topChild = topChild;
            this.bottomChild = bottomChild;
            this.AddChildren(new Position(0, 0), topChild);
            this.AddChildren(new Position(0, this.FrameHeight - bottomChild.FrameHeight), bottomChild);
        }
    }
}

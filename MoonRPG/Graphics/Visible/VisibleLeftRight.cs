﻿using System;
using System.Collections.Generic;

namespace MoonRPG
{
    public abstract class VisibleLeftRight: Visible
    {
        protected Visible leftChild;
        protected Visible rightChild;
        
        public void SetChildren(Visible leftChild, Visible rightChild)
        {
            this.ClearChildren();
            this.leftChild = leftChild;
            this.rightChild = rightChild;
            this.AddChildren(new Position(0, 0), leftChild);
            this.AddChildren(new Position(this.FrameWidth - rightChild.FrameWidth, 0), rightChild);
        }
    }
}

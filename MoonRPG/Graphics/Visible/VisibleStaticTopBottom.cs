﻿using System;
namespace MoonRPG.Graphics.Visible
{
    public class VisibleStaticTopBottom:VisibleTopBottom
    {
        public override int FrameWidth => frameWidth;

        public override int FrameHeight => frameHeight;
        private readonly int frameWidth;
        private readonly int frameHeight;

        public VisibleStaticTopBottom(int? frameWidth, int? frameHeight)
        {
            this.frameWidth = frameWidth ?? ConsoleFrame.maxX;
            this.frameHeight = frameHeight ?? ConsoleFrame.maxY;
        }
    }
}

﻿using System;
namespace MoonRPG
{
    public abstract class VisibleVertical:Visible
    {
        protected int bottom;
        public override int FrameHeight { get => bottom; }

        protected VisibleVertical()
        {
            this.bottom = 0;
        }

        public void AddChildrenToBottom(Visible children)
        {
            this.AddChildren(new Position(0, this.bottom), children);
            this.bottom += children.FrameHeight;
        }
        public override void ClearChildren()
        {
            this.bottom = 0;
            base.ClearChildren();
        }

        protected override void ReloadContent()
        {
            base.ReloadContent();
            if (this.frame.width < this.FrameWidth || this.frame.height < this.FrameHeight)
            {
                Frame currentFrame = this.frame;
                this.frame = new Frame(this.FrameWidth, this.FrameHeight);
                this.frame.PrintFrame(Position.TopLeft, currentFrame);
            }
        }
        /* todo add to this and visible
        public void ClearChildren()
        {
            this.children = ne
        }
        */
    }
}

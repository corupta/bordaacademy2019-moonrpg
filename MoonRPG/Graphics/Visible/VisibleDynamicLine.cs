﻿using System;
namespace MoonRPG
{
    public class VisibleDynamicLine:Visible
    {
        public override int FrameHeight { get => 1; }
        public override int FrameWidth { get => frame?.width ?? 0; }

        protected void ChangeText(string message, Color color = null)
        {
            this.frame = new Frame(new string[] { message }, color);
        }
    }
}

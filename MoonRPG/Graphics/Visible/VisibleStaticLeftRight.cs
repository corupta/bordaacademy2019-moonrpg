﻿using System;
namespace MoonRPG.Graphics.Visible
{
    public class VisibleStaticLeftRight:VisibleLeftRight
    {
        public override int FrameWidth => frameWidth;

        public override int FrameHeight => frameHeight;
        private readonly int frameWidth;
        private readonly int frameHeight;

        public VisibleStaticLeftRight(int? frameWidth, int? frameHeight)
        {
            this.frameWidth = frameWidth ?? ConsoleFrame.maxX;
            this.frameHeight = frameHeight ?? ConsoleFrame.maxY;
        }

        #region TEMPORARY
        protected override void ReloadContent()
        {
            if (this.frame is null)
            {
                this.frame = new Frame(this.FrameWidth, this.FrameHeight, new Color(ConsoleColor.White, ConsoleColor.Green));
            }
        }
        #endregion
    }
}

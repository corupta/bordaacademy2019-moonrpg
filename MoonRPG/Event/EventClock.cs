﻿using System;
using System.Threading;
namespace MoonRPG
{
    public delegate void ClockEventHandler();

    public class EventClock
    {
        private static EventClock instance;
        private static readonly object instanceLock = new object();

        public static EventClock GetInstance()
        {
            if (instance == null)
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new EventClock();
                    }
                }
            }
            return instance;
        }

        private EventClock()
        {
            Thread clockThread = new Thread(this.Run);
            clockThread.Start();
        }

        private void Run()
        {
            while (true)
            {
                Thread.Sleep(15);
                // Thread thread = new Thread(this.MakeTick);
                // thread.Start();
                this.MakeTick();
            }
        }

        private readonly object tickLock = new object();

        private void MakeTick()
        {
            lock (tickLock)
            {
                ClockTick?.Invoke();
            }
        }

        public event ClockEventHandler ClockTick;
    }
}

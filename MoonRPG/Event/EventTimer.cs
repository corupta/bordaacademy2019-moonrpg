﻿using System;
namespace MoonRPG
{
    public delegate void TimerEventHandler();

    public class EventTimer
    {
        public static EventTimer ScheduleAction(int duration, TimerEventHandler timerEventHandler)
        {
            EventTimer eventTimer = new EventTimer(duration);
            eventTimer.TimerFinished += timerEventHandler;
            eventTimer.Start();
            return eventTimer;
        }
        private int duration;
        private EventTimer(int duration)
        {
            this.duration = duration;
        }
        private void Start()
        {
            EventClock.GetInstance().ClockTick += this.Update;
        }
        private void Update()
        {
            if (--this.duration <= 0)
            {
                this.Finish();
            }
        }
        private void Finish(bool invoke = true)
        {
            EventClock.GetInstance().ClockTick -= this.Update;
            if (invoke)
            {
                TimerFinished?.Invoke();
            }
            TimerFinished = null;
        }
        public void Intercept(bool invoke = false)
        {
            this.Finish(invoke);
        }
        public event TimerEventHandler TimerFinished;
    }
}

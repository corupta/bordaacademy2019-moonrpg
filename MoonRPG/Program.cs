﻿using System;
using MoonRPG.Graphics.Visible;
using MoonRPG.Player;
namespace MoonRPG
{
    class Program
    {
        private static BasePlayer humanPlayer;
        private static BasePlayer computerPlayer;

        private static BasePlayer GetHumanPlayer()
        {
            return humanPlayer;
        }
        private static BasePlayer GetComputerPlayer()
        {
            return computerPlayer;
        }

        public static void Main()
        {

            KeyInputter keyInputter = KeyInputter.GetInstance();
            EventClock eventClock = EventClock.GetInstance();
            humanPlayer = new HumanPlayer(GetComputerPlayer);
            computerPlayer = new ComputerPlayer(GetHumanPlayer);

            humanPlayer.Prepare();
            computerPlayer.Prepare();

            Printer printer = Printer.GetInstance();

            VisibleStaticTopBottom visibleTopBottom = new VisibleStaticTopBottom(null, null);
            printer.SetChildren(visibleTopBottom);

            VisibleStaticLeftRight visibleLeftRight = new VisibleStaticLeftRight(null, 16);

            visibleLeftRight.SetChildren(humanPlayer.VisibleCharacter, computerPlayer.VisibleCharacter);

            InformationView informationView = InformationView.GetInstance();

            visibleTopBottom.SetChildren(visibleLeftRight, informationView);

            humanPlayer.NextMove();
        }
    }
}

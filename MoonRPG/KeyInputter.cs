﻿using System;
using System.Threading;
namespace MoonRPG
{
    public delegate void KeyEvent(ConsoleKey key);

    public class KeyInputter
    {
        private static KeyInputter instance;
        private static readonly object instanceLock = new object();

        public static KeyInputter GetInstance()
        {
            if (instance == null)
            {
                lock(instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new KeyInputter();
                    }
                }
            }
            return instance;
        }

        private readonly object keyPressLock = new object();

        private KeyInputter() {
            Thread inputThread = new Thread(this.TrackKeyInputs);
            inputThread.Start();
        }

        private void PressKey(object passedKey)
        {
            ConsoleKey key = (ConsoleKey)passedKey;

            if (key == ConsoleKey.Escape)
            {
                Position.BottomLeft.MoveCursorTo();
                Environment.Exit(0);
            } else
            {
                lock (keyPressLock)
                {
                    this.KeyPressed?.Invoke(key);
                }
            }
        }

        private void TrackKeyInputs()
        {
            while(true)
            {
                ConsoleKey key = Console.ReadKey(true).Key;
                Thread thread = new Thread(this.PressKey);
                thread.Start(key);
            }
        }

        public event KeyEvent KeyPressed;
    }
}

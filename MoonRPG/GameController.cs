﻿using System;
namespace MoonRPG
{
    public class GameController
    {
        private static GameController instance;
        public static GameController GetInstance()
        {
            if (GameController.instance == null)
            {
                GameController.instance = new GameController();
            }
            return GameController.instance;
        }
        private GameController()
        {
        }

        /*

        public bool KeyPress(ConsoleKey key) // return false to finish/stop the game.
        {
            switch(key)
            {
                case ConsoleKey.A:
                    ThrowBall();
                    break;
                case ConsoleKey.B:
                    TurnBall();
                    break;
                case ConsoleKey.Q:
                    PrintMars();
                    break;
                case ConsoleKey.Z:
                    HideMars();
                    break;
                case ConsoleKey.E:
                    PrintJupiter();
                    break;
                case ConsoleKey.C:
                    HideJupiter();
                    break;
                case ConsoleKey.Escape:
                    return false;
            }
            return true;
        }

        public void ThrowBall()
        {
            Animatable animatable = new Animatable(GameController.ball, new Position(5, 10), Animation.LinearRight, 20);
            animatable.Start();
        }

        public void TurnBall()
        {
            Animatable animatable = new Animatable(GameController.ball, new Position(20, 10), Animation.CircleClockWise, 100);
            animatable.Start();
        }

        public void PrintMars()
        {
            Character mars = new MarsSoldier();
            mars.Prepare();
            Printer printer = Printer.GetInstance();

            printer.PrintFrame(Position.LeftPlayer, mars);
        }
        public void HideMars()
        {

            Character mars = new MarsSoldier();
            mars.Prepare();
            mars.HideAll();
            Printer printer = Printer.GetInstance();

            printer.PrintFrame(Position.LeftPlayer, mars);
        }

        public void PrintJupiter()
        {
            Character jupiter = new JupiterSoldier();
            jupiter.Prepare();
            Printer printer = Printer.GetInstance();

            printer.PrintFrame(Position.RightPlayer, jupiter);
        }

        public void HideJupiter()
        {
            Character jupiter = new JupiterSoldier();
            jupiter.Prepare();
            jupiter.HideAll();
            Printer printer = Printer.GetInstance();

            printer.PrintFrame(Position.RightPlayer, jupiter);
        }

        private static Frame ball = new Frame(new string[5]{
                "  /---\\  ",
                " /-----\\ ",
                "|-------|",
                " \\-----/ ",
                "  \\---/  "
                });
                */
    }
}

﻿using System;
namespace MoonRPG.Moves
{
    public class AttackMove:Move
    {
        public override string Description => "Attack: Make a normal attack (0 Mana)";

        public override int ManaRequirement => 0;

        public override void Make(Character caster, Character enemy)
        {
            base.Make(caster, enemy);
            // todo add animation here
            int attackValue = caster.statList.attack.GetRandomAttackValue();
            int damageValue = enemy.statList.defense.GetDamageAfterAttack(attackValue);
            enemy.statList.health.ChangeHealth(-damageValue);
            this.FinishMove(caster, enemy);
        }
    }
}

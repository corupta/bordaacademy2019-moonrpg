﻿using System;
using MoonRPG;
namespace MoonRPG.Moves.Controller
{
    public class RandomMoveController : MoveController
    {
        private static readonly Random random = new Random();
        public RandomMoveController(GetCharacter GetCaster, GetCharacter GetEnemy)
            : base(GetCaster, GetEnemy) { }

        public override void MakeMove()
        {
            int waitDuration = random.Next(10, 40);
            EventTimer.ScheduleAction(waitDuration, this.MakeMoveDirectly);
        }
        private MoveList moves;
        private void MakeMoveDirectly()
        {
            int moveIndex;
            Character caster = GetCaster();
            Character enemy = GetEnemy();
            moves = caster.moveList;
            moves.MoveFinished += this.FinishMove;
            do
            {
                moveIndex = RandomMoveController.random.Next(0, moves.Count);
            } while(moves.GetMove(moveIndex).CanBeCasted(caster, enemy) == false);
            Move move = moves.GetMove(moveIndex);
            move.Make(caster, enemy);
        }

        protected override void FinishMove(Move move, Character caster, Character enemy)
        {
            moves.MoveFinished -= FinishMove;
            base.FinishMove(move, caster, enemy);
        }
    }
}

﻿using System;
using MoonRPG.Graphics.Selectable;
namespace MoonRPG.Moves.Controller
{
    public class SelectableMoveController:MoveController
    {
        private readonly SelectableMoveList selectableMoveList;
        public SelectableMoveController(GetCharacter GetCaster, GetCharacter GetEnemy)
            :base(GetCaster, GetEnemy)
        {
            this.selectableMoveList = new SelectableMoveList(GetCaster, GetEnemy);
            selectableMoveList.OnSelected += this.HandleSelected;
            selectableMoveList.MoveFinished += this.FinishMove;
        }

        public override void MakeMove()
        {
            selectableMoveList.Activate();
        }

        private void HandleSelected(Selectable selected)
        {

        }
    }
}

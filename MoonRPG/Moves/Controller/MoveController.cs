﻿using System;
namespace MoonRPG.Moves.Controller
{

    public abstract class MoveController
    {
        protected GetCharacter GetCaster;
        protected GetCharacter GetEnemy;
        public MoveController(GetCharacter GetCaster, GetCharacter GetEnemy)
        {
            this.GetCaster = GetCaster;
            this.GetEnemy = GetEnemy;
        }
        public abstract void MakeMove();

        protected virtual void FinishMove(Move move, Character caster, Character enemy)
        {
            this.MoveDone?.Invoke(move, caster, enemy);
        }

        public event MoveEvent MoveDone;
    }
}

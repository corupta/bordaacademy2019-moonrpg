﻿using System;
namespace MoonRPG.Moves
{
    public class VisibleMove:VisibleDynamicLine
    {
        private static Color disabledColor = new Color(ConsoleColor.DarkGray);
        private static Color activeColor = new Color(ConsoleColor.White);

        public void ReloadText(Move move, bool disabled)
        {
            Color color = disabled ? disabledColor : activeColor;
            this.ChangeText(move.Description, color);
        }
    }
}

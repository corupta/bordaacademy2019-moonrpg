﻿using System;
using MoonRPG.Graphics.Selectable;
namespace MoonRPG.Moves
{
    public delegate Character GetCharacter();

    public class SelectableMove:Selectable
    {
        private readonly Move move;
        private readonly VisibleMove visibleMove;
        private readonly GetCharacter GetCaster;
        private readonly GetCharacter GetEnemy;
        public SelectableMove(Move move, GetCharacter GetCaster, GetCharacter GetEnemy)
            : this(move, GetCaster, GetEnemy, new VisibleMove()) { }
        private SelectableMove(Move move, GetCharacter GetCaster, GetCharacter GetEnemy, VisibleMove visibleMove)
            : base(visibleMove)
        {
            this.move = move;
            this.visibleMove = visibleMove;
            this.GetCaster = GetCaster;
            this.GetEnemy = GetEnemy;
        }

        /* public void SetCharacters(Character caster, Character enemy)
        {
            this.caster = caster;
            this.enemy = enemy;
        } */

        public override void Prepare()
        {
            Disabled = !move.CanBeCasted(GetCaster(), GetEnemy());
            visibleMove.ReloadText(move, Disabled);
        }

        protected override void HandleSelected()
        {
            this.move.Make(GetCaster(), GetEnemy());
        }
    }
}

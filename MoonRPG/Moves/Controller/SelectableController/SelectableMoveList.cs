﻿using System;
using System.Collections.Generic;
using MoonRPG.Graphics.Selectable;
namespace MoonRPG.Moves
{
    public class SelectableMoveList : SelectableList<SelectableMove>
    {
        private readonly GetCharacter GetCaster;
        private readonly GetCharacter GetEnemy;
        private MoveList moveList;
        public SelectableMoveList(GetCharacter GetCaster, GetCharacter GetEnemy)
        {
            this.GetCaster = GetCaster;
            this.GetEnemy = GetEnemy;
        }

        protected override void Prepare()
        {
            if (GetCaster().moveList != moveList)
            {
                moveList = GetCaster().moveList;
                ClearChildren();
                foreach (Move move in this.moveList)
                {
                    AddSelectable(new SelectableMove(move, GetCaster, GetEnemy));
                }
            }
            InformationView informationView = InformationView.GetInstance();
            foreach (SelectableMove selectableMove in selectables)
            {
                informationView.AddChildrenToBottom(selectableMove.Visible);
            }
            moveList.MoveFinished += FinishMove;
        }

        protected override void Disable()
        {
            base.Disable();
            InformationView informationView = InformationView.GetInstance();
            informationView.ClearChildren();
        }

        private void FinishMove(Move move, Character caster, Character enemy)
        {
            moveList.MoveFinished -= FinishMove;
            MoveFinished?.Invoke(move, caster, enemy);
        }

        public event MoveEvent MoveFinished;
    }
}

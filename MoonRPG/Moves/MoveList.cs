﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MoonRPG.Moves
{
    public class MoveList : IEnumerable<Move>
    {
        private List<Move> moves;
        public int Count { get => moves.Count; }
        public MoveList()
        {
            this.moves = new List<Move>();
        }
        public MoveList(IEnumerable<Move> moves)
        {
            this.moves = new List<Move>(moves);
            this.TrackMoveEvents(moves);
        }
        public void AppendMoves(IEnumerable<Move> newMoves)
        {
            this.moves.AddRange(newMoves);
            this.TrackMoveEvents(newMoves);
        }
        private void TrackMoveEvents(IEnumerable<Move> newMoves)
        {
            foreach (Move move in newMoves)
            {
                move.MoveFinished += FinishMove;
            }
        }

        public IEnumerator<Move> GetEnumerator()
        {
            return ((IEnumerable<Move>)moves).GetEnumerator();
        }

        public Move GetMove(int moveIndex)
        {
            return this.moves[moveIndex];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Move>)moves).GetEnumerator();
        }

        private void FinishMove(Move move, Character caster, Character enemy)
        {
            MoveFinished?.Invoke(move, caster, enemy);
        }

        public event MoveEvent MoveFinished;
    }

}

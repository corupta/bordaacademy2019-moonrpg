﻿using System;
namespace MoonRPG.Moves
{
    public delegate void MoveEvent(Move move, Character caster, Character enemy);
    public abstract class Move
    {
        public abstract int ManaRequirement { get; }
        public abstract string Description { get; }
        public virtual void Make(Character caster, Character enemy)
        {
            caster.statList.mana.ChangeMana(-ManaRequirement);
        }
        public virtual bool CanBeCasted(Character caster, Character enemy) {
            return caster.statList.mana.CanCastMove(ManaRequirement);
        }
        protected virtual void FinishMove(Character caster, Character enemy)
        {
            enemy.statList.mana.Reload();
            MoveFinished?.Invoke(this, caster, enemy);
        }

        public event MoveEvent MoveFinished;
    }
}

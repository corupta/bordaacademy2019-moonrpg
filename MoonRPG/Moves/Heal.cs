﻿using System;
namespace MoonRPG.Moves
{
    public class Heal:Move
    {
        public override int ManaRequirement => 6;

        public override string Description => "Heal: Heals 20 HP (6 Mana)";

        public override void Make(Character caster, Character enemy)
        {
            base.Make(caster, enemy);

            caster.statList.health.ChangeHealth(20);

            this.FinishMove(caster, enemy);
        }
    }
}

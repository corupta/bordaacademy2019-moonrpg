﻿using System;
namespace MoonRPG.Moves
{
    public class DoubleAttackMove:Move
    {
        public DoubleAttackMove()
        {
        }

        public override string Description => "Double Attack: Attacks twice (12 Mana)";

        public override int ManaRequirement => 12;

        public override void Make(Character caster, Character enemy)
        {
            base.Make(caster, enemy);

            EventTimer.ScheduleAction(1, () => this.Hit(caster, enemy, false));

            EventTimer.ScheduleAction(26, () => this.Hit(caster, enemy, true));
        }

        private void Hit(Character caster, Character enemy, bool finishMove)
        {
            int attackValue = caster.statList.attack.GetRandomAttackValue();
            int damageValue = enemy.statList.defense.GetDamageAfterAttack(attackValue);
            enemy.statList.health.ChangeHealth(-damageValue);
            if (finishMove)
            {
                this.FinishMove(caster, enemy);
            }
        }
    }
}

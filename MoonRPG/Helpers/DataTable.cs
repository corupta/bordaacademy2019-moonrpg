﻿using System;
using System.Collections.Generic;

namespace MoonRPG
{
    public abstract class DataTable<T>
    {
        protected abstract T MakeRowData(string[] rowData);

        private Dictionary<string, T> Table;
        private readonly string dataFile;
        protected DataTable(string dataFile)
        {
            this.dataFile = dataFile;
        }
        private void ReadData()
        {
            this.Table = new Dictionary<string, T>();
            string[] rows = FileInputter.ReadAllLines($"Assets/Data/{this.dataFile}.csv");
            foreach (string row in rows)
            {
                string[] rowData = row.Split(", ");
                this.Table[rowData[0]] = this.MakeRowData(rowData);
            }
        }
        public T GetData(string key)
        {
            if (this.Table is null)
            {
                this.ReadData();
            }
            return this.Table[key];
        }
    }
}

﻿using System;
namespace MoonRPG
{
    public class Runner
    {
        private GameController gameController;
        private Printer printer;
        public Runner()
        {
            this.gameController = GameController.GetInstance();
            this.printer = Printer.GetInstance();
        }
        public void Start()
        {
            while (true)
            {
                if (Console.KeyAvailable)
                {
                    ConsoleKey key = Console.ReadKey(true).Key;
                    if (this.animationQueue.IsFinished())
                    {
                        if (this.gameController.KeyPress(key) == false)
                        {
                            return;
                        }
                    }
                    
                }
                this.animationQueue.Next();
                System.Threading.Thread.Sleep(16);
            }
        }
    }
}
